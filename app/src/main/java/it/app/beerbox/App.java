package it.app.beerbox;

import android.app.Application;
import android.content.Context;

import org.codejargon.feather.Feather;

import java.lang.ref.WeakReference;

import it.app.beerbox.di.DataModule;

/**
 * Created by enrico.morotti on 10/05/21
 */
public class App extends Application {

    private static WeakReference<Context> context;
    private static Feather feather;

    @Override
    public void onCreate() {
        super.onCreate();
        App.context = new WeakReference<>(getApplicationContext());
        feather = Feather.with(new DataModule());
    }

    public static Context getAppContext() {
        return App.context.get();
    }

    public static Feather getFeather() {
        return feather;
    }
}
