package it.app.beerbox.di;

import org.codejargon.feather.Provides;

import javax.inject.Singleton;

import it.app.beerbox.data.Repository;
import it.app.beerbox.data.RepositoryImpl;

/**
 * Created by enrico.morotti on 10/05/21
 */
public class DataModule {

    @Provides
    @Singleton
    Repository provideRepository() {
        return new RepositoryImpl();
    }
}
