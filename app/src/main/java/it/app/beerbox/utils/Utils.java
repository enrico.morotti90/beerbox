package it.app.beerbox.utils;

import android.content.res.Resources;

/**
 * Created by enrico.morotti on 09/05/21
 */
public class Utils {

    public static int pxToPd(Resources resources, int value) {
        return (int) (value * resources.getDisplayMetrics().density);
    }
}
