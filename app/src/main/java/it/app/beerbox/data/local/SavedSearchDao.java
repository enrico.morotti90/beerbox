package it.app.beerbox.data.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

import it.app.beerbox.data.model.SavedBeer;
import it.app.beerbox.data.model.SavedSearch;

/**
 * Created by enrico.morotti on 14/05/21
 */
@Dao
public interface SavedSearchDao {

    @Query("SELECT * FROM savedSearch ORDER BY created_at DESC")
    LiveData<List<SavedSearch>> getSavedSearchesLiveData();

    @Query("SELECT * FROM savedSearch ORDER BY created_at DESC")
    List<SavedSearch> getSavedSearches();

    @Query("SELECT * FROM savedSearch WHERE search = :value LIMIT 1")
    SavedSearch findByValue(String value);

    @Insert
    void save(SavedSearch search);

    @Delete
    void delete(List<SavedSearch> searches);
}
