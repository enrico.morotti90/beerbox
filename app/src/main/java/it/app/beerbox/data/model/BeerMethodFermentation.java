package it.app.beerbox.data.model;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerMethodFermentation {
    private BeerAmount temp;

    public BeerAmount getTemp() {
        return temp;
    }

    public void setTemp(BeerAmount temp) {
        this.temp = temp;
    }
}
