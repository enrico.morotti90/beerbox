package it.app.beerbox.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;
import java.util.Objects;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class Beer {
    private String id;
    private String name;
    private String tagline;
    @SerializedName("first_brewed")
    private String firstBrewed;
    private String description;
    private String image_url;
    private Float abv;
    private Float ibu;
    @SerializedName("target_fg")
    private Float targetFg;
    @SerializedName("target_og")
    private Float targetOg;
    private Float ebc;
    private Float srm;
    private Float ph;
    @SerializedName("attenuation_level")
    private Float attenuationLevel;
    private BeerAmount volume;
    @SerializedName("boil_volume")
    private BeerAmount boilVolume;
    private BeerMethod method;
    private BeerIngredients ingredients;
    @SerializedName("food_pairing")
    private List<String> foodPairing;
    @SerializedName("brewers_tips")
    private String brewersTips;
    @SerializedName("contributed_by")
    private String contributedBy;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTagline() {
        return tagline;
    }

    public void setTagline(String tagline) {
        this.tagline = tagline;
    }

    public String getFirstBrewed() {
        return firstBrewed;
    }

    public void setFirstBrewed(String firstBrewed) {
        this.firstBrewed = firstBrewed;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImage_url() {
        return image_url;
    }

    public void setImage_url(String image_url) {
        this.image_url = image_url;
    }

    public Float getAbv() {
        return abv;
    }

    public void setAbv(Float abv) {
        this.abv = abv;
    }

    public Float getIbu() {
        return ibu;
    }

    public void setIbu(Float ibu) {
        this.ibu = ibu;
    }

    public Float getTargetFg() {
        return targetFg;
    }

    public void setTargetFg(Float targetFg) {
        this.targetFg = targetFg;
    }

    public Float getTargetOg() {
        return targetOg;
    }

    public void setTargetOg(Float targetOg) {
        this.targetOg = targetOg;
    }

    public Float getEbc() {
        return ebc;
    }

    public void setEbc(Float ebc) {
        this.ebc = ebc;
    }

    public Float getSrm() {
        return srm;
    }

    public void setSrm(Float srm) {
        this.srm = srm;
    }

    public Float getPh() {
        return ph;
    }

    public void setPh(Float ph) {
        this.ph = ph;
    }

    public Float getAttenuationLevel() {
        return attenuationLevel;
    }

    public void setAttenuationLevel(Float attenuationLevel) {
        this.attenuationLevel = attenuationLevel;
    }

    public BeerAmount getVolume() {
        return volume;
    }

    public void setVolume(BeerAmount volume) {
        this.volume = volume;
    }

    public BeerAmount getBoilVolume() {
        return boilVolume;
    }

    public void setBoilVolume(BeerAmount boilVolume) {
        this.boilVolume = boilVolume;
    }

    public BeerMethod getMethod() {
        return method;
    }

    public void setMethod(BeerMethod method) {
        this.method = method;
    }

    public BeerIngredients getIngredients() {
        return ingredients;
    }

    public void setIngredients(BeerIngredients ingredients) {
        this.ingredients = ingredients;
    }

    public List<String> getFoodPairing() {
        return foodPairing;
    }

    public void setFoodPairing(List<String> foodPairing) {
        this.foodPairing = foodPairing;
    }

    public String getBrewersTips() {
        return brewersTips;
    }

    public void setBrewersTips(String brewersTips) {
        this.brewersTips = brewersTips;
    }

    public String getContributedBy() {
        return contributedBy;
    }

    public void setContributedBy(String contributedBy) {
        this.contributedBy = contributedBy;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Beer beer = (Beer) o;
        return id.equals(beer.id) && name.equals(beer.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name);
    }
}
