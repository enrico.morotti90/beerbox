package it.app.beerbox.data.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerAmount {
    private Float value;
    private String unit;

    public Float getValue() {
        return value;
    }

    public void setValue(Float value) {
        this.value = value;
    }

    public String getUnit() {
        return unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    @NotNull
    @Override
    public String toString() {
        return value + " " + unit;
    }
}
