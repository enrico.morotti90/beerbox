package it.app.beerbox.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

/**
 * Created by enrico.morotti on 14/05/21
 */
@Entity(tableName = "savedBeer")
public class SavedBeer {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "beer_id")
    public String beerId;
}
