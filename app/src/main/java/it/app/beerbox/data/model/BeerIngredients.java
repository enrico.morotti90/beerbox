package it.app.beerbox.data.model;

import java.util.List;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerIngredients {
    private List<BeerIngredient> malt;
    private List<BeerIngredient> hops;
    private String yeast;

    public List<BeerIngredient> getMalt() {
        return malt;
    }

    public void setMalt(List<BeerIngredient> malt) {
        this.malt = malt;
    }

    public List<BeerIngredient> getHops() {
        return hops;
    }

    public void setHops(List<BeerIngredient> hops) {
        this.hops = hops;
    }

    public String getYeast() {
        return yeast;
    }

    public void setYeast(String yeast) {
        this.yeast = yeast;
    }
}
