package it.app.beerbox.data.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerMethod {
    @SerializedName("mash_temp")
    private List<BeerMethodMash> mashTemp;
    private BeerMethodFermentation fermentation;
    private String twist;

    public List<BeerMethodMash> getMashTemp() {
        return mashTemp;
    }

    public void setMashTemp(List<BeerMethodMash> mashTemp) {
        this.mashTemp = mashTemp;
    }

    public BeerMethodFermentation getFermentation() {
        return fermentation;
    }

    public void setFermentation(BeerMethodFermentation fermentation) {
        this.fermentation = fermentation;
    }

    public String getTwist() {
        return twist;
    }

    public void setTwist(String twist) {
        this.twist = twist;
    }
}
