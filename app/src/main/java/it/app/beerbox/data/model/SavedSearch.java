package it.app.beerbox.data.model;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import org.jetbrains.annotations.NotNull;

/**
 * Created by enrico.morotti on 14/05/21
 */
@Entity(tableName = "savedSearch")
public class SavedSearch {

    @PrimaryKey
    @NotNull
    @ColumnInfo(name = "search")
    public String search;

    @ColumnInfo(name = "created_at")
    public long createdAt;
}
