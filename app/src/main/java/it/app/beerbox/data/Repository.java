package it.app.beerbox.data;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import it.app.beerbox.data.model.Beer;
import it.app.beerbox.data.model.SavedBeer;
import it.app.beerbox.data.model.SavedSearch;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public interface Repository {

    CompletableFuture<List<Beer>> getBeers(Integer page, String searchName);

    LiveData<SavedBeer> getFavoriteBeer(String id);

    void toggleFavoriteBeer(String id);

    LiveData<List<SavedSearch>> getSavedSearches();

    void saveSearch(String value);
}
