package it.app.beerbox.data.network;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import it.app.beerbox.data.model.Beer;
import retrofit2.http.GET;
import retrofit2.http.Query;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public interface PunkApiClient {

    @GET("beers")
    CompletableFuture<List<Beer>> getBeers(@Query("page") int page, @Query("per_page") int perPage, @Query("beer_name") String beerName);
}
