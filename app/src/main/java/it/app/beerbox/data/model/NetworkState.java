package it.app.beerbox.data.model;

/**
 * Created by enrico.morotti on 09/05/21
 */
public enum NetworkState {
    LOADING, LOADED, ERROR
}
