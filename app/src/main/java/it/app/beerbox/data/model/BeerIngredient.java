package it.app.beerbox.data.model;

import org.jetbrains.annotations.NotNull;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerIngredient {
    private String name;
    private BeerAmount amount;
    private String add;
    private String attribute;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BeerAmount getAmount() {
        return amount;
    }

    public void setAmount(BeerAmount amount) {
        this.amount = amount;
    }

    public String getAdd() {
        return add;
    }

    public void setAdd(String add) {
        this.add = add;
    }

    public String getAttribute() {
        return attribute;
    }

    public void setAttribute(String attribute) {
        this.attribute = attribute;
    }

    @NotNull
    @Override
    public String toString() {
        return name + " (" + amount + ")";
    }
}
