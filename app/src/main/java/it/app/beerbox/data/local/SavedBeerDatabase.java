package it.app.beerbox.data.local;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import it.app.beerbox.data.model.SavedBeer;
import it.app.beerbox.data.model.SavedSearch;

/**
 * Created by enrico.morotti on 14/05/21
 */
@Database(entities = {SavedBeer.class, SavedSearch.class}, version = 2)
public abstract class SavedBeerDatabase  extends RoomDatabase {
    public abstract SavedBeerDao savedBeerDao();
    public abstract SavedSearchDao savedSearchDao();
}
