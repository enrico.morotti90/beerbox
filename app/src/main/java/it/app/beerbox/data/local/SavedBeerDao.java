package it.app.beerbox.data.local;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;

import it.app.beerbox.data.model.SavedBeer;

/**
 * Created by enrico.morotti on 14/05/21
 */
@Dao
public interface SavedBeerDao {

    @Query("SELECT * FROM savedBeer WHERE beer_id = :id LIMIT 1")
    LiveData<SavedBeer> findByIdLiveData(String id);

    @Query("SELECT * FROM savedBeer WHERE beer_id = :id LIMIT 1")
    SavedBeer findById(String id);

    @Insert
    void save(SavedBeer beer);

    @Delete
    void delete(SavedBeer beer);
}
