package it.app.beerbox.data;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.room.Room;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.CompletableFuture;

import it.app.beerbox.App;
import it.app.beerbox.data.local.SavedBeerDao;
import it.app.beerbox.data.local.SavedBeerDatabase;
import it.app.beerbox.data.local.SavedSearchDao;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.data.model.SavedBeer;
import it.app.beerbox.data.model.SavedSearch;
import it.app.beerbox.data.network.PunkApiClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class RepositoryImpl implements Repository {

    private final PunkApiClient punkApiClient;
    private final SavedBeerDao savedBeerDao;
    private final SavedSearchDao savedSearchDao;

    public RepositoryImpl() {
        this.punkApiClient = new Retrofit.Builder()
                .baseUrl("https://api.punkapi.com/v2/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PunkApiClient.class);
        SavedBeerDatabase db = Room.databaseBuilder(App.getAppContext(),
                SavedBeerDatabase.class, "saved_beer_db")
                .addMigrations(new Migration(1, 2) {
                    @Override
                    public void migrate(@NonNull @NotNull SupportSQLiteDatabase database) {
                        database.execSQL("CREATE TABLE `savedSearch` (`search` TEXT NOT NULL, `created_at` BIGINT NOT NULL, PRIMARY KEY(`search`))");
                    }
                }).build();
        savedBeerDao = db.savedBeerDao();
        savedSearchDao = db.savedSearchDao();
    }

    @Override
    public CompletableFuture<List<Beer>> getBeers(Integer page, String searchName) {
        return punkApiClient.getBeers(page, 25, searchName);
    }

    @Override
    public LiveData<SavedBeer> getFavoriteBeer(String id) {
        return savedBeerDao.findByIdLiveData(id);
    }

    @Override
    public void toggleFavoriteBeer(String id) {
        SavedBeer beer = savedBeerDao.findById(id);
        if (beer == null) {
            SavedBeer newBeer = new SavedBeer();
            newBeer.beerId = id;
            savedBeerDao.save(newBeer);
        } else {
            savedBeerDao.delete(beer);
        }
    }

    @Override
    public LiveData<List<SavedSearch>> getSavedSearches() {
        return savedSearchDao.getSavedSearchesLiveData();
    }

    @Override
    public void saveSearch(String value) {
        SavedSearch search = savedSearchDao.findByValue(value);
        if (search == null) {
            SavedSearch newSearch = new SavedSearch();
            newSearch.search = value;
            newSearch.createdAt = System.currentTimeMillis();
            savedSearchDao.save(newSearch);
        }
        List<SavedSearch> searchList = savedSearchDao.getSavedSearches();
        if (searchList.size() > 6) {
            savedSearchDao.delete(searchList.subList(6, searchList.size() - 1));
        }
    }
}
