package it.app.beerbox.data.model;

/**
 * Created by enrico.morotti on 05/05/2021
 */
public class BeerMethodMash {
    private BeerAmount temp;
    private Integer duration;

    public BeerAmount getTemp() {
        return temp;
    }

    public void setTemp(BeerAmount temp) {
        this.temp = temp;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }
}
