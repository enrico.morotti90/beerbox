package it.app.beerbox.ui.beers.home;

import android.graphics.Typeface;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import it.app.beerbox.R;
import it.app.beerbox.data.model.NetworkState;
import it.app.beerbox.databinding.HomeFragmentBinding;
import it.app.beerbox.ui.beers.BeersViewModel;
import it.app.beerbox.ui.beers.details.DetailsModalFragment;
import it.app.beerbox.ui.beers.home.adapter.PagingAdapter;
import it.app.beerbox.ui.beers.home.adapter.SavedSearchListAdapter;
import it.app.beerbox.ui.beers.home.view.SearchView;
import it.app.beerbox.utils.ListItemDecorator;
import it.app.beerbox.utils.Utils;

public class HomeFragment extends Fragment {

    private HomeFragmentBinding binding;
    private BeersViewModel viewModel;
    private PagingAdapter adapter;
    private SavedSearchListAdapter savedSearchListAdapter;

    public static HomeFragment newInstance() {
        return new HomeFragment();
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = HomeFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(BeersViewModel.class);

        viewModel.networkState.observe(getViewLifecycleOwner(), this::onNetworkStateChange);
        viewModel.beerList.observe(getViewLifecycleOwner(), beers -> adapter.submitList(beers));
        viewModel.savedSearchList.observe(getViewLifecycleOwner(), list -> {
            savedSearchListAdapter.updateItemList(list);
            if (!list.isEmpty()) {
                binding.savedSearchRecyclerView.setVisibility(View.VISIBLE);
            }
        });
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void initializeView() {
        // Title
        String startTitle = getString(R.string.home_title_start);
        String endTitle = getString(R.string.home_title_end);
        SpannableString title = new SpannableString(String.format("%s %s", startTitle, endTitle));
        title.setSpan(new StyleSpan(Typeface.BOLD), startTitle.length() + 1, title.length(), Spanned.SPAN_INCLUSIVE_EXCLUSIVE);
        binding.titleTxt.setText(title);

        // List
        adapter = new PagingAdapter(beer -> {
            viewModel.setSelectedBeer(beer);
            if (getFragmentManager() != null) {
                DetailsModalFragment.newInstance().show(getFragmentManager(), "details_dialog_fragment");
            }
        });
        binding.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        binding.recyclerView.addItemDecoration(new ListItemDecorator(Utils.pxToPd(getResources(), 16), ContextCompat.getDrawable(Objects.requireNonNull(getContext()), R.drawable.list_item_divider)));
        binding.recyclerView.setAdapter(adapter);

        // Saved search list
        savedSearchListAdapter = new SavedSearchListAdapter(search -> {
            binding.searchView.updateText(search);
        });
        binding.savedSearchRecyclerView.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        binding.savedSearchRecyclerView.setAdapter(savedSearchListAdapter);

        // SearchView
        binding.searchView.setListener(new SearchView.SearchViewListener() {
            @Override
            public void onTextChange(String text) {
                viewModel.reloadList(text);
                if (getActivity() != null) {
                    getActivity().runOnUiThread(() -> savedSearchListAdapter.resetSelectedSearch(text));
                }
            }

            @Override
            public void onDoneTapped(String text) {
                if (text != null && !text.isEmpty()) {
                    viewModel.saveSearch(text);
                }
            }
        });
    }

    private void onNetworkStateChange(NetworkState state) {
        switch (state) {
            case LOADING:
                adapter.setLoadingMore(true);
                break;
            case LOADED:
                adapter.setLoadingMore(false);
                break;
            case ERROR:
                Toast.makeText(getContext(), "Error", Toast.LENGTH_SHORT).show();
                break;
        }
    }
}