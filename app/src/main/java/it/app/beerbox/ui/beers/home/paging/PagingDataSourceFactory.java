package it.app.beerbox.ui.beers.home.paging;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;

import org.jetbrains.annotations.NotNull;

import it.app.beerbox.data.Repository;
import it.app.beerbox.data.model.Beer;

/**
 * Created by enrico.morotti on 06/05/21
 */
public class PagingDataSourceFactory extends DataSource.Factory<Integer, Beer> {

    private final Repository repository;
    private final MutableLiveData<PagingDataSource> dataSourceLiveData = new MutableLiveData<>();

    private String searchName;

    public PagingDataSourceFactory(Repository repository) {
        this.repository = repository;
    }

    @NotNull
    @Override
    public DataSource<Integer, Beer> create() {
        PagingDataSource dataSource = new PagingDataSource(repository, searchName);
        dataSourceLiveData.postValue(dataSource);
        return dataSource;
    }

    public MutableLiveData<PagingDataSource> getDataSourceLiveData() {
        return dataSourceLiveData;
    }

    public void setSearchName(String searchName) {
        if (searchName == null || searchName.isEmpty()) {
            this.searchName = null;
        } else {
            this.searchName = searchName;
        }
        if (dataSourceLiveData.getValue() != null) {
            dataSourceLiveData.getValue().invalidate();
        }
    }
}
