package it.app.beerbox.ui.beers;

import android.os.AsyncTask;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.Transformations;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PagedList;

import java.util.List;

import it.app.beerbox.App;
import it.app.beerbox.data.Repository;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.data.model.NetworkState;
import it.app.beerbox.data.model.SavedBeer;
import it.app.beerbox.data.model.SavedSearch;
import it.app.beerbox.ui.beers.home.paging.PagingDataSource;
import it.app.beerbox.ui.beers.home.paging.PagingDataSourceFactory;

public class BeersViewModel extends ViewModel {

    public LiveData<PagedList<Beer>> beerList;
    public LiveData<NetworkState> networkState;

    private final Repository repository;
    private final PagingDataSourceFactory dataSourceFactory;

    public LiveData<List<SavedSearch>> savedSearchList;

    public LiveData<SavedBeer> selectedBeerFavorite;
    private Beer selectedBeer;

    public BeersViewModel() {
        repository = App.getFeather().instance(Repository.class);
        dataSourceFactory = new PagingDataSourceFactory(repository);

        networkState = Transformations.switchMap(dataSourceFactory.getDataSourceLiveData(), PagingDataSource::getNetworkState);

        PagedList.Config config = new PagedList.Config.Builder()
                .setPageSize(25)
                .setPrefetchDistance(50)
                .setEnablePlaceholders(false)
                .build();
        beerList = new LivePagedListBuilder<>(dataSourceFactory, config).build();

        savedSearchList = repository.getSavedSearches();
    }

    public void reloadList(String text) {
        dataSourceFactory.setSearchName(text);
    }

    public Beer getSelectedBeer() {
        return selectedBeer;
    }

    public void setSelectedBeer(Beer selectedBeer) {
        this.selectedBeer = selectedBeer;
        selectedBeerFavorite = repository.getFavoriteBeer(selectedBeer.getId());
    }

    public void toggleFavoriteBeer() {
        AsyncTask.execute(() -> {
            repository.toggleFavoriteBeer(selectedBeer.getId());
            selectedBeerFavorite = repository.getFavoriteBeer(selectedBeer.getId());
        });
    }

    public void saveSearch(String text) {
        AsyncTask.execute(() -> {
            repository.saveSearch(text);
            savedSearchList = repository.getSavedSearches();
        });
    }
}