package it.app.beerbox.ui.beers.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.bumptech.glide.Glide;

import org.jetbrains.annotations.NotNull;

import it.app.beerbox.R;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.utils.Utils;

/**
 * Created by enrico.morotti on 09/05/21
 */
public class BeerView extends ConstraintLayout {

    private TextView nameTxt;
    private TextView typeTxt;
    private TextView descriptionTxt;
    private TextView moreInfoTxt;
    private ImageView imageView;

    private OnBeerClickListener listener;

    public BeerView(@NonNull @NotNull Context context) {
        super(context);
        initialize();
    }

    public BeerView(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BeerView(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public BeerView(@NonNull @NotNull Context context, @Nullable @org.jetbrains.annotations.Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        inflate(getContext(), R.layout.item_beer_view, this);
        setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        int padding = Utils.pxToPd(getResources(), 16);
        setPadding(padding, padding, padding, padding);
        TypedValue outValue = new TypedValue();
        getContext().getTheme().resolveAttribute(android.R.attr.selectableItemBackground, outValue, true);
        setBackgroundResource(outValue.resourceId);

        nameTxt = findViewById(R.id.item_beer_name);
        typeTxt = findViewById(R.id.item_beer_type);
        descriptionTxt = findViewById(R.id.item_beer_description);
        moreInfoTxt = findViewById(R.id.item_beer_more_info);
        imageView = findViewById(R.id.item_beer_img);
    }

    public void setListener(OnBeerClickListener listener) {
        this.listener = listener;
    }

    public void updateData(Beer beer) {
        nameTxt.setText(beer.getName());
        typeTxt.setText(beer.getTagline());
        descriptionTxt.setText(beer.getDescription());
        moreInfoTxt.setText(getContext().getString(R.string.beer_show_more));

        if (beer.getImage_url() == null) {
            Glide.with(this).clear(imageView);
        } else {
            Glide.with(this).load(beer.getImage_url()).into(imageView);
        }

        setOnClickListener(v -> {
            if (listener != null) {
                listener.onBeerClick(beer);
            }
        });
    }

    public interface OnBeerClickListener {
        void onBeerClick(Beer beer);
    }
}
