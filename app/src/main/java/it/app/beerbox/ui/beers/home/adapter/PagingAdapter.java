package it.app.beerbox.ui.beers.home.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import it.app.beerbox.R;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.ui.beers.home.view.BeerView;

/**
 * Created by enrico.morotti on 06/05/21
 */
public class PagingAdapter extends PagedListAdapter<Beer, RecyclerView.ViewHolder> {

    enum LayoutType {
        item(0), loading(R.layout.item_loading_view);

        private final int value;

        LayoutType(int value) {
            this.value = value;
        }
    }

    private boolean isLoadingMore = false;
    private final OnItemClickListener listener;

    public PagingAdapter(OnItemClickListener listener) {
        super(new DiffUtil.ItemCallback<Beer>() {
            @Override
            public boolean areItemsTheSame(@NonNull @NotNull Beer oldItem, @NonNull @NotNull Beer newItem) {
                return oldItem.getId().equals(newItem.getId());
            }

            @Override
            public boolean areContentsTheSame(@NonNull @NotNull Beer oldItem, @NonNull @NotNull Beer newItem) {
                return oldItem.equals(newItem);
            }
        });
        this.listener = listener;
    }

    @Override
    public int getItemViewType(int position) {
        if (isLoadingMore && position == getItemCount() - 1) return LayoutType.loading.value;
        return LayoutType.item.value;
    }

    @Override
    public int getItemCount() {
        return super.getItemCount() + (isLoadingMore ? 1 : 0);
    }

    @NonNull
    @NotNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        if (viewType == LayoutType.item.value) {
            return new BeerViewHolder(new BeerView(parent.getContext()), listener);
        } else {
            return new LoadingViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(viewType, parent, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof BeerViewHolder) {
            ((BeerViewHolder) holder).bind(getItem(position));
        }
    }

    public void setLoadingMore(boolean loadingMore) {
        isLoadingMore = loadingMore;
        notifyDataSetChanged();
    }

    // View holders

    private static class BeerViewHolder extends RecyclerView.ViewHolder {
        private final OnItemClickListener listener;

        public BeerViewHolder(@NonNull @NotNull BeerView itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
        }

        void bind(Beer beer) {
            ((BeerView) itemView).setListener(beer1 -> {
                if (listener != null) {
                    listener.onItemClick(beer1);
                }
            });
            ((BeerView) itemView).updateData(beer);
        }
    }

    private static class LoadingViewHolder extends RecyclerView.ViewHolder {
        public LoadingViewHolder(@NonNull @NotNull View itemView) {
            super(itemView);
        }
    }

    // Interface

    public interface OnItemClickListener {
        void onItemClick(Beer beer);
    }
}
