package it.app.beerbox.ui.beers.home.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import it.app.beerbox.R;

/**
 * Created by enrico.morotti on 06/05/21
 */
public class BannerView extends FrameLayout {
    public BannerView(@NonNull Context context) {
        super(context);
        initialize();
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public BannerView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        inflate(getContext(), R.layout.view_banner, this);
        setBackgroundResource(R.drawable.banner_view_background);
    }
}
