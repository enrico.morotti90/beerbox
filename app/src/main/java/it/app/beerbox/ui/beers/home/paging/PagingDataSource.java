package it.app.beerbox.ui.beers.home.paging;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.PageKeyedDataSource;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.concurrent.ExecutionException;

import it.app.beerbox.data.Repository;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.data.model.NetworkState;

/**
 * Created by enrico.morotti on 06/05/21
 */
public class PagingDataSource extends PageKeyedDataSource<Integer, Beer> {

    private final Repository repository;
    private final MutableLiveData<NetworkState> networkState = new MutableLiveData<>();
    private final String searchName;

    public PagingDataSource(Repository repository, String searchName) {
        this.repository = repository;
        this.searchName = searchName;
    }

    @Override
    public void loadAfter(@NotNull LoadParams<Integer> loadParams, @NotNull LoadCallback<Integer, Beer> loadCallback) {
        networkState.postValue(NetworkState.LOADING);
        try {
            List<Beer> list = repository.getBeers(loadParams.key, searchName).get();
            loadCallback.onResult(list, list.isEmpty() ? null : loadParams.key + 1);
            networkState.postValue(NetworkState.LOADED);
        } catch (ExecutionException | InterruptedException e) {
            networkState.postValue(NetworkState.ERROR);
        }
    }

    @Override
    public void loadBefore(@NotNull LoadParams<Integer> loadParams, @NotNull LoadCallback<Integer, Beer> loadCallback) {
    }

    @Override
    public void loadInitial(@NotNull LoadInitialParams<Integer> loadInitialParams, @NotNull LoadInitialCallback<Integer, Beer> loadInitialCallback) {
        networkState.postValue(NetworkState.LOADING);
        try {
            List<Beer> list = repository.getBeers(1, searchName).get();
            loadInitialCallback.onResult(list, null, list.isEmpty() ? null : 2);
            networkState.postValue(NetworkState.LOADED);
        } catch (ExecutionException | InterruptedException e) {
            networkState.postValue(NetworkState.ERROR);
        }
    }

    public MutableLiveData<NetworkState> getNetworkState() {
        return networkState;
    }
}
