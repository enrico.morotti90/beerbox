package it.app.beerbox.ui.beers.home.view;

import android.content.Context;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.Timer;
import java.util.TimerTask;

import it.app.beerbox.R;

/**
 * Created by enrico.morotti on 10/05/21
 */
public class SearchView extends FrameLayout {

    private EditText searchTxt;
    private ImageButton clearBtn;
    private SearchViewListener listener;

    private Timer timer;

    public SearchView(@NonNull Context context) {
        super(context);
        initialize();
    }

    public SearchView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public SearchView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public SearchView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        inflate(getContext(), R.layout.view_search, this);
        setBackgroundResource(R.drawable.search_view_background);

        searchTxt = findViewById(R.id.search_edit_txt);
        clearBtn = findViewById(R.id.search_clear_btn);

        searchTxt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (timer != null) {
                    timer.cancel();
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
                clearBtn.setVisibility(s == null || s.toString().isEmpty() ? View.GONE : View.VISIBLE);
                timer = new Timer();
                timer.schedule(new TimerTask() {
                    @Override
                    public void run() {
                        if (listener != null) {
                            listener.onTextChange(s != null ? s.toString() : null);
                        }
                    }
                }, 300);
            }
        });
        searchTxt.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH
                    || actionId == EditorInfo.IME_ACTION_DONE
                    || event.getAction() == KeyEvent.ACTION_DOWN
                    && event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
                if (listener != null) {
                    listener.onDoneTapped(searchTxt.getText().toString());
                }
            }
            return false;
        });
        clearBtn.setOnClickListener(v -> searchTxt.setText(null));
    }

    public void setListener(SearchViewListener listener) {
        this.listener = listener;
    }

    public void updateText(String text) {
        searchTxt.setText(text);
        searchTxt.setSelection(text.length());
    }

    public interface SearchViewListener {
        void onTextChange(String text);

        void onDoneTapped(String text);
    }
}
