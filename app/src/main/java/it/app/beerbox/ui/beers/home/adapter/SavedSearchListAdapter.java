package it.app.beerbox.ui.beers.home.adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import it.app.beerbox.data.model.SavedSearch;
import it.app.beerbox.ui.beers.home.view.SavedSearchView;

/**
 * Created by enrico.morotti on 14/05/21
 */
public class SavedSearchListAdapter extends RecyclerView.Adapter<SavedSearchListAdapter.SavedSearchViewHolder> {

    private static String selectedSearch = null;

    private List<SavedSearch> itemList = new ArrayList<>();
    private final OnItemClickListener listener;

    public SavedSearchListAdapter(OnItemClickListener listener) {
        this.listener = search -> {
            if (selectedSearch != null && selectedSearch.equals(search)) {
                selectedSearch = null;
                listener.onItemClick(null);
            } else {
                selectedSearch = search;
                listener.onItemClick(search);
            }
            notifyDataSetChanged();
        };
    }

    @NonNull
    @NotNull
    @Override
    public SavedSearchViewHolder onCreateViewHolder(@NonNull @NotNull ViewGroup parent, int viewType) {
        return new SavedSearchViewHolder(new SavedSearchView(parent.getContext()), listener);
    }

    @Override
    public void onBindViewHolder(@NonNull @NotNull SavedSearchViewHolder holder, int position) {
        holder.bind(itemList.get(position).search);
    }

    @Override
    public int getItemCount() {
        return itemList.size();
    }

    public void updateItemList(List<SavedSearch> itemList) {
        this.itemList = itemList;
        notifyDataSetChanged();
    }

    public void resetSelectedSearch(String text) {
        if(selectedSearch != null && !selectedSearch.equals(text)) {
            selectedSearch = null;
            notifyDataSetChanged();
        }
    }

    // View holders

    protected static class SavedSearchViewHolder extends RecyclerView.ViewHolder {
        private final OnItemClickListener listener;

        public SavedSearchViewHolder(@NonNull @NotNull SavedSearchView itemView, OnItemClickListener listener) {
            super(itemView);
            this.listener = listener;
        }

        void bind(String search) {
            ((SavedSearchView) itemView).setListener(search1 -> {
                if (listener != null) {
                    listener.onItemClick(search1);
                }
            });
            ((SavedSearchView) itemView).updateState(search, selectedSearch != null && selectedSearch.equals(search));
        }
    }

    // Interface

    public interface OnItemClickListener {
        void onItemClick(String search);
    }
}
