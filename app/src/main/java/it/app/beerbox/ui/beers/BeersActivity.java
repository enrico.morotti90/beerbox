package it.app.beerbox.ui.beers;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import it.app.beerbox.R;
import it.app.beerbox.ui.beers.home.HomeFragment;

public class BeersActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.beers_activity);
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.container, HomeFragment.newInstance())
                    .commitNow();
        }
    }
}