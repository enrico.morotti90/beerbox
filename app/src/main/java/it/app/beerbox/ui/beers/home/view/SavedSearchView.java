package it.app.beerbox.ui.beers.home.view;

import android.content.Context;
import android.graphics.Color;
import android.util.AttributeSet;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.content.ContextCompat;

import it.app.beerbox.R;

/**
 * Created by enrico.morotti on 14/05/21
 */
public class SavedSearchView extends FrameLayout {

    private TextView searchTxt;
    private OnSearchClickListener listener;

    public SavedSearchView(@NonNull Context context) {
        super(context);
        initialize();
    }

    public SavedSearchView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public SavedSearchView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public SavedSearchView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        inflate(getContext(), R.layout.item_saved_search_view, this);
        searchTxt = findViewById(R.id.item_search_txt);

        setOnClickListener(v -> {
            if (listener != null) {
                listener.onSearchClick(searchTxt.getText().toString());
            }
        });
    }

    public void setListener(OnSearchClickListener listener) {
        this.listener = listener;
    }

    public void updateState(String text, boolean selected) {
        searchTxt.setText(text);
        if (selected) {
            searchTxt.setBackgroundResource(R.drawable.saved_search_tab_selected);
            searchTxt.setTextColor(Color.BLACK);
        } else {
            searchTxt.setBackgroundResource(R.drawable.saved_search_tab);
            searchTxt.setTextColor(ContextCompat.getColor(getContext(), R.color.search_view));
        }
    }

    public interface OnSearchClickListener {
        void onSearchClick(String search);
    }
}
