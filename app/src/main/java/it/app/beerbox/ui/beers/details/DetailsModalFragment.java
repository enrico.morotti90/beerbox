package it.app.beerbox.ui.beers.details;

import android.app.Dialog;
import android.content.res.Resources;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;
import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.bottomsheet.BottomSheetDialogFragment;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import it.app.beerbox.R;
import it.app.beerbox.data.model.Beer;
import it.app.beerbox.databinding.DetailsFragmentBinding;
import it.app.beerbox.ui.beers.BeersViewModel;
import it.app.beerbox.utils.Utils;

/**
 * Created by enrico.morotti on 11/05/21
 */
public class DetailsModalFragment extends BottomSheetDialogFragment {

    private static final int offset = Utils.pxToPd(Resources.getSystem(), 24);
    private static final int peekHeight = Utils.pxToPd(Resources.getSystem(), 250);

    private DetailsFragmentBinding binding;
    private BeersViewModel viewModel;
    private View bottomGradientView;

    public static DetailsModalFragment newInstance() {
        return new DetailsModalFragment();
    }

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, R.style.ThemeOverlay_Details_BottomSheetDialog);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding = DetailsFragmentBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @NonNull
    @NotNull
    @Override
    public Dialog onCreateDialog(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        bottomSheetDialog.getBehavior().setPeekHeight(peekHeight);
        bottomSheetDialog.setOnShowListener(dialog -> {
            FrameLayout containerLayout = ((BottomSheetDialog) dialog).findViewById(com.google.android.material.R.id.container);
            if (containerLayout != null) {
                bottomGradientView = new View(getContext());
                bottomGradientView.setLayoutParams(new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, offset));
                ((FrameLayout.LayoutParams) bottomGradientView.getLayoutParams()).gravity = Gravity.BOTTOM;
                bottomGradientView.setBackgroundResource(R.drawable.details_view_bottom_gradient);
                containerLayout.addView(bottomGradientView);
            }
        });
        bottomSheetDialog.getBehavior().addBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull @NotNull View bottomSheet, int newState) {
                if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                    binding.closeFab.show();
                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    binding.closeFab.hide();
                }
            }

            @Override
            public void onSlide(@NonNull @NotNull View bottomSheet, float slideOffset) {
                if (slideOffset <= 0) {
                    bottomGradientView.setTranslationY(-peekHeight * slideOffset);
                }
            }
        });
        return bottomSheetDialog;
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initializeView();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        viewModel = new ViewModelProvider(Objects.requireNonNull(getActivity())).get(BeersViewModel.class);
        viewModel.selectedBeerFavorite.observe(getViewLifecycleOwner(), beer -> {
            binding.favBtn.setImageResource(beer != null ? R.drawable.ic_baseline_favorite_24 : R.drawable.ic_baseline_favorite_border_24);
            binding.favBtn.setVisibility(View.VISIBLE);
        });

        showInfo();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
    }

    private void initializeView() {
        binding.closeFab.hide();
        binding.closeFab.setOnClickListener(v -> {
            BottomSheetDialog bottomSheetDialog = (BottomSheetDialog) getDialog();
            if (bottomSheetDialog != null) {
                bottomSheetDialog.getBehavior().setState(BottomSheetBehavior.STATE_HIDDEN);
            }
        });

        binding.favBtn.setVisibility(View.GONE);
        binding.favBtn.setOnClickListener(v -> viewModel.toggleFavoriteBeer());
    }

    private void showInfo() {
        Beer beer = viewModel.getSelectedBeer();
        binding.beerName.setText(beer.getName());
        binding.beerType.setText(beer.getTagline());
        binding.beerDescription.setText(beer.getDescription());

        if (beer.getImage_url() == null) {
            Glide.with(this).clear(binding.beerImg);
        } else {
            Glide.with(this).load(beer.getImage_url()).into(binding.beerImg);
        }

        binding.beerFirstBrewed.showInfo(getString(R.string.detail_first_brewed), beer.getFirstBrewed());
        binding.beerBrewersTips.showInfo(getString(R.string.detail_brewers_tips), beer.getBrewersTips());
        binding.beerContributedBy.showInfo(getString(R.string.detail_contributed_by), beer.getContributedBy());

        binding.beerAbv.showInfo(getString(R.string.detail_abv), beer.getAbv());
        binding.beerIbu.showInfo(getString(R.string.detail_ibu), beer.getIbu());
        binding.beerTargetFg.showInfo(getString(R.string.detail_target_fg), beer.getTargetFg());
        binding.beerTargetOg.showInfo(getString(R.string.detail_target_og), beer.getTargetOg());
        binding.beerEbc.showInfo(getString(R.string.detail_ebc), beer.getEbc());
        binding.beerSrm.showInfo(getString(R.string.detail_srm), beer.getSrm());
        binding.beerPh.showInfo(getString(R.string.detail_ph), beer.getPh());
        binding.beerAttenuationLevel.showInfo(getString(R.string.detail_attenuation_level), beer.getAttenuationLevel());

        binding.beerMalt.showInfo(getString(R.string.detail_malt), TextUtils.join(", ", beer.getIngredients().getMalt()));
        binding.beerHops.showInfo(getString(R.string.detail_hops), TextUtils.join(", ", beer.getIngredients().getHops()));
        binding.beerYeast.showInfo(getString(R.string.detail_yeast), beer.getIngredients().getYeast());
    }
}
