package it.app.beerbox.ui.beers.details.view;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;

import java.util.Locale;

import it.app.beerbox.R;

/**
 * Created by enrico.morotti on 14/05/21
 */
public class DetailBeerInfoRowView extends LinearLayout {

    private TextView titleTxt;
    private TextView valueTxt;

    public DetailBeerInfoRowView(Context context) {
        super(context);
        initialize();
    }

    public DetailBeerInfoRowView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        initialize();
    }

    public DetailBeerInfoRowView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        initialize();
    }

    public DetailBeerInfoRowView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        initialize();
    }

    private void initialize() {
        inflate(getContext(), R.layout.view_beer_info_row, this);
        titleTxt = findViewById(R.id.title_txt);
        valueTxt = findViewById(R.id.value_txt);
    }

    public void showInfo(String title, String value) {
        titleTxt.setText(title);
        valueTxt.setText(value);
    }

    public void showInfo(String title, Float value) {
        titleTxt.setText(title);
        valueTxt.setText(String.format(Locale.getDefault(), "%.1f", value));
    }
}
